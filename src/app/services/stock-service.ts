import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

export type StockItem = {
  symbol: string;
  price: number;
  size: number;
  time: number;
  id: string;
};

const allowedStockSymbols = [
  'SNAP',
  'MSFT',
  'FB',
  'C',
  'AIG',
  'AMZN',
  'BAC',
  'AAPL',
  'GE',
];

@Injectable({
  providedIn: 'root',
})
export class StockService {
  public stockItemsList$: BehaviorSubject<Array<StockItem>> =
    new BehaviorSubject<Array<StockItem>>([]);

  public searchStockItems(search: string): void {
    let error = false;

    const symbols = search.split(',').map((value) => value.trim());

    symbols.forEach((value) => {
      if (!allowedStockSymbols.includes(value)) {
        error = true;
      }
    });

    if (!error) {
      this._http
        .get<StockItem[]>(
          `https://api.iextrading.com/1.0/tops/last?symbols=${symbols.join(
            ','
          )}`
        )
        .pipe(
          map((items) => {
            return items.map((item) => ({
              ...item,
              id: Math.random().toString(36).substring(7),
            }));
          })
        )
        .subscribe((stockItems) => {
          this.stockItemsList$.next(stockItems);
        });
    }
  }

  constructor(private _http: HttpClient) {}
}
