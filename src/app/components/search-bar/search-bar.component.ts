import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StockItem, StockService } from 'src/app/services/stock-service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {
  public searchForm: FormGroup = this._fb.group({
    search: this._fb.control(''),
  });

  constructor(private _fb: FormBuilder, private _stockService: StockService) {}

  ngOnInit(): void {}

  public search() {
    this._stockService.searchStockItems(this.searchForm.value.search);
  }
}
