import { Component, OnInit } from '@angular/core';
import { StockItem, StockService } from 'src/app/services/stock-service';

@Component({
  selector: 'app-stock-items-list',
  templateUrl: './stock-items-list.component.html',
  styleUrls: ['./stock-items-list.component.scss'],
})
export class StockItemsListComponent implements OnInit {
  constructor(private _stockService: StockService) {}

  public stockItems: Array<StockItem> = [];

  ngOnInit(): void {
    const itemsFromLS = JSON.parse(localStorage.getItem('stockItems'));

    if (itemsFromLS) {
      this.stockItems = itemsFromLS;
    }

    this._stockService.stockItemsList$.subscribe((items) => {
      const filteredItems = items.filter((item) => {
        return !this.stockItems.find((stockItem) => {
          return (
            stockItem.price === item.price &&
            stockItem.symbol === item.symbol &&
            stockItem.size === item.size &&
            stockItem.time === item.time
          );
        });
      });

      this.stockItems = [...this.stockItems, ...filteredItems];
    });
  }

  public removeItem(item: StockItem) {
    this.stockItems = this.stockItems.filter((currentItem) => {
      return currentItem.id !== item.id;
    });
  }

  public saveProfile() {
    localStorage.setItem('stockItems', JSON.stringify(this.stockItems));
  }
}
